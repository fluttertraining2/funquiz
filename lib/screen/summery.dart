import 'package:flutter/material.dart';
import 'package:fun_quiz/screen/summery/question_identifier.dart';
import 'package:fun_quiz/screen/summery/summery_item.dart';

class summery_screen extends StatelessWidget {
  summery_screen({super.key, required this.summeryData});

  List<Map<String, Object>> summeryData = [];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      child: SingleChildScrollView(
        child: Column(
          children: summeryData.map((e) {
            return summery_item(e);
          }).toList(),
        ),
      ),
    );
  }
}
