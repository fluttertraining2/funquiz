// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:fun_quiz/data/questions.dart';
import 'package:fun_quiz/screen/summery.dart';
import 'package:google_fonts/google_fonts.dart';

class resultScreen extends StatelessWidget {
  const resultScreen({
    super.key,
    required this.chosenAns,
    required this.onRestart,
  });

  final List<String> chosenAns;
  final void Function() onRestart;

  List<Map<String, Object>> getSummery() {
    List<Map<String, Object>> summeryData = [];

    for (var i = 0; i < chosenAns.length; i++) {
      summeryData.add({
        'questionIndex': i,
        'question': questionsData[i].text,
        'chosenAns': chosenAns[i],
        'correctAns': questionsData[i].answers[0],
      });
    }
    return summeryData;
  }

  @override
  Widget build(BuildContext context) {
    final getsummery = getSummery();
    final numQuestions = questionsData.length;
    final numCorrect = getsummery.where((data) {
      return data['chosenAns'] == data['correctAns'];
    }).length;

    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              "you answered $numCorrect out of $numQuestions questions correctly",
              style: GoogleFonts.lato(
                color: Colors.white,
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 30,
            ),
            summery_screen(summeryData: getsummery),
            const SizedBox(
              height: 30,
            ),
            TextButton.icon(
              onPressed: onRestart,
              icon: const Icon(Icons.refresh),
              label: const Text(
                "Restart",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
