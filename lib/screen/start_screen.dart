// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

class startScreen extends StatelessWidget {
  const startScreen(this.quizhandler, {super.key});

  final Function() quizhandler;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            'assets/images/quiz-logo.png',
            width: 200,
            color: Color.fromARGB(62, 255, 255, 255),
          ),
          const SizedBox(
            height: 50,
          ),
          const Text(
            "Learn Flutter in Fun way",
            style: TextStyle(
              fontSize: 25,
              color: Colors.white,
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          OutlinedButton.icon(
            onPressed: quizhandler,
            style: OutlinedButton.styleFrom(
              foregroundColor: Colors.white,
              // textStyle: const TextStyle(fontSize: 20),
            ),
            icon: const Icon(Icons.arrow_right_alt),
            label: const Text('Start'),
          )
        ],
      ),
    );
  }
}
