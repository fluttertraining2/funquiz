// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:fun_quiz/screen/answer_button.dart';
import 'package:fun_quiz/data/questions.dart';
import 'package:google_fonts/google_fonts.dart';

class questions extends StatefulWidget {
  const questions({super.key, required this.onSelectAns});

  final void Function(String answer) onSelectAns;

  @override
  State<questions> createState() => _questionsState();
}

class _questionsState extends State<questions> {
  var qusIndex = 0;

  void setQusIndex(String selectedAns) {
    widget.onSelectAns(selectedAns);
    setState(() {
      qusIndex++;
      // print(qusIndex);
    });
  }

  @override
  Widget build(BuildContext context) {
    final currentQues = questionsData[qusIndex];

    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              currentQues.text,
              style: GoogleFonts.lato(
                color: Colors.white,
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 30,
            ),
            ...currentQues.getSuffledAns().map(
              (ans) {
                return answerBtn(
                  ansTxt: ans,
                  onTab: () {
                    setQusIndex(ans);
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
