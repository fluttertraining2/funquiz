// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';

class questionIdentifier extends StatelessWidget {
  const questionIdentifier(
      {super.key, required this.queIndex, required this.isCorrect});

  final int queIndex;
  final bool isCorrect;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 30,
      height: 30,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: isCorrect ? Colors.blue : Colors.red,
        borderRadius: BorderRadius.circular(100),
      ),
      child: Text(
        (queIndex + 1).toString(),
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),
      ),
    );
  }
}
