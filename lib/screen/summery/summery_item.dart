import 'package:flutter/material.dart';
import 'package:fun_quiz/screen/summery/question_identifier.dart';
import 'package:google_fonts/google_fonts.dart';

class summery_item extends StatelessWidget {
  const summery_item(this.idemData, {super.key});

  final Map<String, Object> idemData;

  @override
  Widget build(BuildContext context) {
    final isCorrect = idemData['chosenAns'] == idemData['correctAns'];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.only(right: 10),
            child: questionIdentifier(
              queIndex: (idemData['questionIndex'] as int),
              isCorrect: isCorrect,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  idemData['question'] as String,
                  style: GoogleFonts.lato(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  idemData['chosenAns'] as String,
                  style: GoogleFonts.lato(
                    color: const Color.fromARGB(255, 235, 59, 205),
                  ),
                ),
                Text(
                  idemData['correctAns'] as String,
                  style: GoogleFonts.lato(
                    color: const Color.fromARGB(255, 59, 240, 98),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
