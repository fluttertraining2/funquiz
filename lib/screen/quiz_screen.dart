// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:fun_quiz/data/questions.dart';
import 'package:fun_quiz/screen/questions_screen.dart';
import 'package:fun_quiz/screen/result_screen.dart';
import 'package:fun_quiz/screen/start_screen.dart';

class quiz extends StatefulWidget {
  const quiz({super.key});

  @override
  State<quiz> createState() => _quizState();
}

class _quizState extends State<quiz> {
  List<String> answers = [];

  var activeScreen = 'start-screen'; // startScreen(switchScreen);;

  void switchScreen() {
    setState(() {
      activeScreen = 'question-screen'; //const questions();
    });
  }

  void ansQus(String ans) {
    answers.add(ans);

    if (answers.length == questionsData.length) {
      setState(() {
        activeScreen = 'result-screen';
      });
    }
  }

  void restart() {
    setState(() {
      answers = [];
      activeScreen = 'question-screen';
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget screenWidget = startScreen(switchScreen);

    if (activeScreen == 'question-screen') {
      screenWidget = questions(
        onSelectAns: ansQus,
      );
    }
    if (activeScreen == 'result-screen') {
      screenWidget = resultScreen(
        chosenAns: answers,
        onRestart: restart,
      );
    }
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 5, 25, 92),
                Color.fromARGB(255, 11, 35, 114),
                Color.fromARGB(255, 19, 47, 141),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          child: screenWidget,
        ),
      ),
    );
  }
}
