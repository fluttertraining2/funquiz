class QuizQuestion {
  const QuizQuestion(this.text, this.answers);

  final String text;
  final List<String> answers;

  List<String> getSuffledAns() {
    final shuffledAns = List.of(answers);
    shuffledAns.shuffle();
    return shuffledAns;
  }
}
